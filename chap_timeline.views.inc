<?php

/**
 * Implementation of hook_views_plugins().
 *
 * Provide a style plugin and a row plugin to render timeline items.
 */
function chap_timeline_views_plugins() {
  return array(
    'style' => array(
      'chap_timeline' => array(
        'title' => t('CHAP Timeline'),
        'help' => t('Display the view as a CHAP library timeline.'),
        'theme' => 'chap_timeline',
        'handler' => 'chap_timeline_plugin_style_timeline',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'uses grouping' => TRUE,
        'type' => 'normal',
      ),
    ),
    'row' => array(
      'chap_timeline_row' => array(
        'title' => t('CHAP Timeline Row'),
        'help' => t('Display the row as a CHAP timeline item.'),
        'theme' => 'chap_timeline_row',
        'handler' => 'chap_timeline_plugin_row_timeline',
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
