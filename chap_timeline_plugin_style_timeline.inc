<?php
/**
 * Implementation of chap_timeline_plugin_style_timeline.
 **/
class chap_timeline_plugin_style_timeline extends views_plugin_style {
  /**
   * Set default options
   **/
  function option_definition() {
    $options = parent::option_definition();
    $options['general']['style'] = array('default' => 'box');
    $options['general']['box.align'] = array('default' => 'center');
    $options['general']['locale'] = array('default' => 'en');
    $options['dim']['width'] = array('default' => '100%');
    $options['dim']['height'] = array('default' => 'auto');
    $options['dim']['minHeight'] = array('default' => 0);
    $options['dim']['eventMargin'] = array('default' => 10);
    $options['dim']['eventMarginAxis'] = array('default' => 10);
    $options['dim']['dragAreaWidth'] = array('default' => 10);
    $options['dim']['groupsWidth'] = array('default' => '');

    $options['dates']['min'] = array('default' => '2012-01-01 00:00:00');
    $options['dates']['max'] = array('default' => '2030-12-31 00:00:00');
    $options['dates']['start'] = array('default' => '');
    $options['dates']['end'] = array('default' => '');
    $options['general']['scale'] = array('default' => 'links.Timeline.StepDate.SCALE.HOUR');
    $options['general']['step'] = array('default' => '1');

    $options['animate'] = array('default' => TRUE);
    $options['animateZoom'] = array('default' => TRUE);
    $options['axisOnTop'] = array('default' => FALSE);
    $options['cluster'] = array('default' => FALSE);
    $options['groupsOnRight'] = array('default' => FALSE);
    $options['selectable'] = array('default' => TRUE);
    $options['showCurrentTime'] = array('default' => TRUE);
    $options['showCustomTime'] = array('default' => FALSE);
    $options['showMajorLabels'] = array('default' => TRUE);
    $options['showMinorLabels'] = array('default' => TRUE);
    $options['showNavigation'] = array('default' => FALSE);
    $options['snapEvents'] = array('default' => TRUE);
    $options['stackEvents'] = array('default' => TRUE);
    $options['unselectable'] = array('default' => TRUE);
    $options['zoomable'] = array('default' => TRUE);
    $options['zoomMax'] = array('default' => 315360000000000);
    $options['zoomMin'] = array('default' => 10);

    return $options;
  }
  
  /**
   * Provide a form for setting options
   **/
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['general'] = array(
      '#title' => t('General Style Settings'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['general']['style'] = array(
      '#weight' => -10,
      '#title' => t('Style'),
      '#type' => 'select',
      '#options' => array('box' => t('Box'), 'dot' => t('Dot')),
      '#default_value' => $this->options['general']['style'],
    );
    $form['general']['box.align'] = array(
      '#title' => t('Box Alignment'),
      '#type' => 'select',
      '#options' => array('center' => t('Center'), 'left' => t('Left'), 'right' => t('Right')),
      '#default_value' => $this->options['general']['box.align'],
    );
    $form['general']['scale'] = array(
      '#title' => t('Scale'),
      '#type' => 'select',
      '#default_value' => $this->options['general']['scale'],
      '#options' => array(
        'links.Timeline.StepDate.SCALE.MILLISECOND' => t('Milliseconds'),
        'links.Timeline.StepDate.SCALE.SECOND' => t('Seconds'),
        'links.Timeline.StepDate.SCALE.MINUTE' => t('Minutes'),
        'links.Timeline.StepDate.SCALE.HOUR' => t('Hours'),
        'links.Timeline.StepDate.SCALE.WEEKDAY' => t('Weekdays'),
        'links.Timeline.StepDate.SCALE.DAY' => t('Days'),
        'links.Timeline.StepDate.SCALE.MONTH' => t('Months'),
        'links.Timeline.StepDate.SCALE.YEAR' => t('Years'),
      ),
    );
    $form['general']['step'] = array(
      '#title' => t('Step'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dates']['step'],
    );
    $form['general']['locale'] = array(
      '#title' => t('Locale'),
      '#type' => 'textfield',
      '#default_value' => $this->options['general']['locale'],
    );


    $form['dim'] = array(
      '#type' => 'fieldset',
      '#title' => t('Dimensions'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['dim']['width'] = array(
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['width'],
    );
    $form['dim']['height'] = array(
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['height'],
    );
    $form['dim']['minHeight'] = array(
      '#title' => t('Min Height'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['minHeight'],
    );
    $form['dim']['eventMargin'] = array(
      '#title' => t('Event Margin'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['eventMargin'],
    );
    $form['dim']['eventMarginAxis'] = array(
      '#title' => t('Event Margin Axis'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['eventMarginAxis'],
    );
    $form['dim']['dragAreaWidth'] = array(
      '#title' => t('Drag Area Width'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['dragAreaWidth'],
    );
    $form['dim']['groupsWidth'] = array(
      '#title' => t('Groups Width'),
      '#type' => 'textfield',
      '#default_value' => $this->options['dim']['groupsWidth'],
    );

    $form['dates'] = array(
      '#type' => 'fieldset',
      '#title' => t('Date Settings'),
    );

    $form['dates']['start'] = array(
      '#title' => t('Start Date'),
      '#type' => 'date_text',
      '#default_value' => $this->options['dates']['start'],
      '#date_format' => 'Y-m-d',
    );
    $form['dates']['end'] = array(
      '#title' => t('End Date'),
      '#type' => 'date_text',
      '#default_value' => $this->options['dates']['end'],
      '#date_format' => 'Y-m-d',
    );
    $form['dates']['min'] = array(
      '#title' => t('Min Date'),
      '#type' => 'date_text',
      '#default_value' => $this->options['dates']['min'],
      '#date_format' => 'Y-m-d',
    );
    $form['dates']['max'] = array(
      '#title' => t('Max Date'),
      '#type' => 'date_text',
      '#default_value' => $this->options['dates']['max'],
      '#date_format' => 'Y-m-d',
    );

    $form['animate'] = array(
      '#title' => t('Animate'),
      '#type' => 'checkbox',
      '#description' => t('When true, events are moved animated when resizing or moving them. This is very pleasing for the eye, but does require more computational power.'),
      '#default_value' => $this->options['animate'],
    );
    $form['animateZoom'] = array(
      '#title' => t('Animate Zoom'),
      '#type' => 'select',
      '#options' => array(TRUE => t('Yes'), FALSE => t('No')),
      '#default_value' => $this->options['animateZoom'],
    );
    $form['axisOnTop'] = array(
      '#title' => t('Axis On Top'),
      '#type' => 'select',
      '#options' => array(TRUE => t('Yes'), FALSE => t('No')),
      '#default_value' => $this->options['axisOnTop'],
    );
    $form['cluster'] = array(
      '#title' => t('Cluster'),
      '#type' => 'select',
      '#options' => array(TRUE => t('Yes'), FALSE => t('No')),
      '#default_value' => $this->options['cluster'],
    );
    $form['general']['groupsOnRight'] = array(
      '#title' => t('Groups On Right'),
      '#description' => t('If false, the groups legend is drawn at the left side of the timeline. If true, the groups legend is drawn on the right side.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['general']['groupsOnRight'],
    );
    $form['selectable'] = array(
      '#title' => t('Selectable'),
      '#type' => 'select',
      '#options' => array(TRUE => t('Yes'), FALSE => t('No')),
      '#default_value' => $this->options['selectable'],
    );
  }

  /**
   * Pre-render routine: include javascript libraries, etc.
   * */
  function pre_render($result) {
    drupal_set_html_head('<script type="text/javascript" src="http://www.google.com/jsapi"></script>');
    drupal_add_js(libraries_get_path('chap-timeline') . '/timeline.js', 'module');
    drupal_add_css(libraries_get_path('chap-timeline') . '/timeline.css', 'module');

    # Setup options
    $options = array('grouping' => $this->options['grouping']);

    # Fieldset groups
    foreach (array('general', 'dim') as $group) {
      foreach ($this->options[$group] as $key => $val) {
        if ($this->options[$group][$key]) {
          $options[$key] = $val;
        }
      }
    }

    # Dates
    foreach ($this->options['dates'] as $key => $val) {
      if (!is_array($val)) {
        $options[$key] = $val;
      }
    }

    # Non-fieldset options
    foreach (array('animate', 'animateZoom', 'axisOnTop', 'cluster', 'groupsOnRight', 'selectable') as $key) {
      if ($options[$key]) {
        $options[$key] = $this->options[$key];
      }
    }

    drupal_add_js(array('chap_timeline' => array('options' => $options)), 'setting');
    drupal_add_js(drupal_get_path('module', 'chap_timeline') . '/chap_timeline.js');
  }

  /**
   * Render the timeline
   **/
  function render() {
    if ($this->uses_row_plugin() && empty($this->row_plugin)) {
      vpr('chap_timeline_plugin_style_timeline: Missing row plugin');
      return;
    }

    // Group the rows
    if (!empty($this->options['grouping'])) {
      $sets = $this->render_grouping($this->view->result, $this->options['grouping']);

      foreach ($sets as $group => $records) {
        if ($this->uses_row_plugin()) {
          foreach ($records as $row_index => $row) {
            $this->view->row_index = $row_index;
            $rows[$group][$row_index]['title'] = $this->row_plugin->render($row);
            $rows[$group][$row_index]['dates'] = $this->_daterange($row->node_data_field_vision_timeframe_field_vision_timeframe_value);
          }
          unset($this->view->row_index);
        } else {
          $rows[$title] = $records;
        }
      }
    } else {
      $rows = $this->render_grouping($this->view->result, $this->options['grouping']);
      $rows = $rows['']; # If no grouping, there's only one entry

      if ($this->uses_row_plugin()) {
        foreach ($rows as $row_index => $row) {
          $this->view->row_index = $row_index;
          $rows[$row_index]['title'] = $this->row_plugin->render($row);
          $rows[$row_index]['dates'] = $this->_daterange($row->node_data_field_vision_timeframe_field_vision_timeframe_value);
        }
        unset($this->view->row_index);
      }
    }

    drupal_add_js(array('chap_timeline' => array('rows' => $rows)), 'setting');
    $output = theme($this->theme_functions(), $this->view, $this->options, $rows);
    return $output;
  }

  function _daterange($value) {
    switch ($value) {
    case 'short':
      $dates = array('start' => '2012 01 01 00:00:00', 'end' => '2014 12 31 00:00:00');
      break;
    case 'medium':
      $dates = array('start' => '2015 01 01 00:00:00', 'end' => '2019 12 31 00:00:00');
      break;
    case 'long':
      $dates = array('start' => '2020 01 01 00:00:00', 'end' => '2029 12 31 00:00:00');
      break;
    case 'all':
      $dates = array('start' => '2012 01 01 00:00:00', 'end' => '2029 12 31 00:00:00');
      break;
    }
    return $dates;
  }
}
?>
