Drupal.chap_timeline = Drupal.chap_timeline || {}

// Setup the google visualization API to call our custom drawTimeline function
Drupal.behaviors.chap_timeline = function (context) {
  // The setTimeout ensures the google.load doesn't call doc.write and obliterate the DOM.
  setTimeout(function() { 
    google.load("visualization","1", 
      {"callback": Drupal.chap_timeline.drawTimeline}); 
  }, 0); // The timeout is 0 so it happens right away.
};

// Custom drawTimeline function, which sets up the DataTable,
// feeds it with row data from the view, and renders the timeline.
Drupal.chap_timeline.drawTimeline = function() {
  var settings = Drupal.settings.chap_timeline.options;
  if (settings["start"]) { settings["start"] = new Date(settings["start"]); }
  if (settings["end"]) { settings["end"] = new Date(settings["end"]); }
  if (settings["min"]) { settings["min"] = new Date(settings["min"]); }
  if (settings["max"]) { settings["max"] = new Date(settings["max"]); }

  var data = new google.visualization.DataTable();

  // Basic columns
  data.addColumn('datetime', 'start');
  data.addColumn('datetime', 'end');
  data.addColumn('string', 'content');

  // Is there a grouping field?
  if (settings["grouping"]) {
    // Add it to the columns in the DataTable
    data.addColumn('string', 'group');

    // Loop through groups of rows
    for (var group in Drupal.settings.chap_timeline.rows) {
      var rows = Drupal.settings.chap_timeline.rows[group];
      for (var i in rows) {
        start = new Date(rows[i]["dates"]["start"]);
        end = new Date(rows[i]["dates"]["end"]);
        data.addRow([start, end, rows[i]["title"], group]);
      }
    }
  } else {
    // Loop through rows
    var rows = Drupal.settings.chap_timeline.rows;
    for (i = 0 ; i < rows.length ; i++) {
      start = new Date(rows[i]["dates"]["start"]);
      end = new Date(rows[i]["dates"]["start"]);
      data.addRow([start, end, rows[i]]);
    }
  }

  // Grab the element with id chap-timeline
  timeline = new links.Timeline(document.getElementById('chap-timeline'));
  // Render the DataTable as a timeline in that element.
  timeline.draw(data, settings);
};
